#!/bin/bash

# Create symlinks
ln -s ~/.dot_overrides/vim/vimrc.local        ~/.vimrc.local
ln -s ~/.dot_overrides/vim/vimrc.plugins      ~/.vimrc.plugins
ln -s ~/.dot_overrides/vim/vimrc.theme        ~/.vimrc.theme
ln -s ~/.dot_overrides/tmux/tmux.conf.local   ~/.tmux.conf.local
ln -s ~/.dot_overrides/fish/config.fish.local ~/.config.fish.local
